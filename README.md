# Auctionhouse

This is a school project with deadline of 14 october 2019.
in this project i'm going to try and create a workable version of the WOW classic auctionhouse.

# Setup

To run this program you need to use the Included SQL statment to create the relevant database,
in AbstractDAO you might want to edit the url, username and password to match the connection to your MySQL database.

Default username: "root"
Default password: ""
Default Port: 3306
Default database on the port: "wow"


**WARNING: Please run the Unit-tests before launching the program.**

the tests provide some basic data to the database for demonstration and testing use, 
The auctiontype needs to be filled in with data for the program to work propperly, else it will proclaim SQLExceptions as the database will check on this table for existing data.
& the unit tests incidentially also do just that.

**notice**

This project has a .bat file thats used in the powerpoint, launching this bat file launches the project