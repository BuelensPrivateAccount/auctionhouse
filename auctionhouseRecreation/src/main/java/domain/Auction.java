package domain;

import java.util.Date;

/**
 * This is the object of a Auction.
 * @see DomainInterface
 */
public class Auction implements DomainInterface {
    private int id;
    private String itemName;
    private int amount;
    private Date postageDate;
    private int buyoutPrice;
    private int currentMinimalBid;
    private int buyerID;
    private int sellerID;
    private int type;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return getItemName();
    }

    @Override
    public void setName(String s) {
        setItemName(s);
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getPostageDate() {
        return postageDate;
    }

    public void setPostageDate(Date postageDate) {
        this.postageDate = postageDate;
    }

    public int getBuyoutPrice() {
        return buyoutPrice;
    }

    public void setBuyoutPrice(int buyoutPrice) {
        this.buyoutPrice = buyoutPrice;
    }

    public int getCurrentMinimalBid() {
        return currentMinimalBid;
    }

    public void setCurrentMinimalBid(int currentMinimalBid) {
        this.currentMinimalBid = currentMinimalBid;
    }

    public int getBuyerID() {
        return buyerID;
    }

    public void setBuyerID(int buyerID) {
        this.buyerID = buyerID;
    }

    public int getSellerID() {
        return sellerID;
    }

    public void setSellerID(int sellerID) {
        this.sellerID = sellerID;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Auction{" +
                "id=" + id +
                ", itemName='" + itemName + '\'' +
                ", amount=" + amount +
                ", postageDate=" + postageDate +
                ", buyoutPrice= " + moneyToGoldSilverCopper(buyoutPrice)[0] + " gold " + moneyToGoldSilverCopper(buyoutPrice)[1] + " silver " + moneyToGoldSilverCopper(buyoutPrice)[2]+ " copper " +
                ", currentMinimalBid= " + moneyToGoldSilverCopper(currentMinimalBid)[0] + " gold " + moneyToGoldSilverCopper(currentMinimalBid)[1] + " silver " + moneyToGoldSilverCopper(currentMinimalBid)[2]+ " copper " +
                ", buyerID=" + buyerID +
                ", sellerID=" + sellerID +
                ", type=" + type +
                '}';
    }
    
    private String[] moneyToGoldSilverCopper(int moneyint){
        String money = Integer.toString(moneyint);
        String copper;
        String silver = "0";
        String gold = "0";
        if(money.length() >1){
            copper = money.substring(money.length() - 2);
        } else{
            copper = money;
        }
        if(money.length() >3){
            silver = money.substring(money.length()-4,money.length()-2);
            gold = money.substring(0,money.length()-4);
        }
        String[] moneyString  = new String[3];
        moneyString[0] = gold;
        moneyString[1] = silver;
        moneyString[2] = copper;
         return moneyString;
        
    }
}
