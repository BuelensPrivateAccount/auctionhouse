package domain;

import java.math.BigInteger;

/**
 * This is the object of a Player.
 * @see DomainInterface
 */
public class Player implements DomainInterface{
    private int id;
    private String name;
    private String faction;
    private String password;
    private long money;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", faction='" + faction + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
