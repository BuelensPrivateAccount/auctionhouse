package repo;

import java.sql.*;

import java.sql.SQLException;

/**
 * This Abstract class exists to have a overarching Url, username and password to use with the database by every single DAO
 * There's also a abstract method here that says that every DAO-objects has a implementation of SetToData.
 */
abstract class AbstractDAO {
    private static String username = "root";
    private static String password = "";
    static Connection connection;
    private static String hostname = "localhost";
    private static String port = "3306";
    private static final String url = "jdbc:mysql://" + hostname + ":" + port + "/wow";

    public static void setHostname(String hostname) {
        AbstractDAO.hostname = hostname;
    }

    public static void setPort(String port) {
        AbstractDAO.port = port;
    }

    public static String getHostname() {
        return hostname;
    }

    public static String getPort() {
        return port;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        AbstractDAO.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        AbstractDAO.password = password;
    }

    /**
     * This method will open a the {@link Connection} connection.
     *
     * @throws SQLException sql handling.
     */
    static void OpenConnection() throws SQLException {
        connection = DriverManager.getConnection(url, username, password);
    }

    /**
     * this method will close the {@link Connection} connection.
     *
     * @throws SQLException sql handling.
     */
    static void CloseConnection() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    /**
     * This method will take the data from a resultSet and turn it into a object to be used
     *
     * @param set this is the resultSet gotten by a method and makes a usable Object with it
     * @return {@link Object} specifically a object using the interface: {@link domain.DomainInterface}
     * @throws SQLException As we're working with SQL there's always need to keep SQLException in mind.
     */
    abstract protected Object setToData(ResultSet set) throws SQLException;

}

