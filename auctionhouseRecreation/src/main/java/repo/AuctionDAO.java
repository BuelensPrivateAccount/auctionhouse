package repo;

import domain.Auction;
import exceptions.GotDarnITFuckingUserException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class AuctionDAO extends AbstractDAO {
    public static final String ALLIANCE = "alliance_ah";
    public static final String HORDE = "horde_ah";
    public static final String NEUTRAL = "neutral_ah";

    /**
     * this method checks if the string handed to it exactly matches one of the 3 strings from this class.
     *
     * @param toValidate the input we match against the static variables in here
     *                   !!DesignerNote: If there's work in AuctionDAO run the input through this in every method!!
     * @throws GotDarnITFuckingUserException this get's thrown when the user doesn't play nice and uses one of the Static final strings defined here.
     */
    private void correctAHValidation(String toValidate) throws GotDarnITFuckingUserException {
        if (!(ALLIANCE == toValidate || HORDE == toValidate || NEUTRAL == toValidate)) {
            throw new GotDarnITFuckingUserException("Please use AuctionDAO.ALLIANCE/AuctionDAO.HORDE/AuctionDAO.NEUTRAL");
        }
    }

    /**
     * this method finds all auctions on the specific ah it's been tasked to look on.
     *
     * @param type the specific Ah to look on
     * @return List of {@link Auction}
     * @throws SQLException                  sql handling
     * @throws GotDarnITFuckingUserException thrown when type does not match the static final values
     */
    public List<Auction> findAllAuctions(String type) throws SQLException, GotDarnITFuckingUserException {
        correctAHValidation(type);
        List<Auction> auctions = new ArrayList<>();
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + type);
        statement.execute();
        ResultSet set = statement.getResultSet();
        while (set.next()) {
            auctions.add((Auction) setToData(set));
        }
        CloseConnection();
        statement.close();
        return auctions;
    }

    /**
     * this method is used to look for a specific item in the list of auctions
     *
     * @param query the query the user uses to search on
     * @param type  the specific Ah to look on
     * @return List of {@link Auction}
     * @throws SQLException                  sql handling
     * @throws GotDarnITFuckingUserException thrown when type does not match the static final values
     */
    public List<Auction> findAuctionsByQueryLikeness(String type, String query) throws SQLException, GotDarnITFuckingUserException {
        correctAHValidation(type);
        query = "%" + query + "%";
        return findAuctionsByQuery(type, query);
    }

    /**
     * this method is used to look for a specific item in the list of auctions
     *
     * @param query the query the user uses to search on
     * @param type  the specific Ah to look on
     * @return List of {@link Auction}
     * @throws SQLException                  sql handling
     * @throws GotDarnITFuckingUserException thrown when type does not match the static final values
     */
    private List<Auction> findAuctionsByQuery(String type, String query) throws SQLException, GotDarnITFuckingUserException {
        correctAHValidation(type);
        List<Auction> auctions = new ArrayList<>();
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + type + " WHERE ItemName Like ?");
        statement.setString(1, query);
        statement.execute();
        ResultSet set = statement.getResultSet();
        if (set.next()) {
            auctions.add((Auction) setToData(set));
        }
        while (set.next()) {
            auctions.add((Auction) setToData(set));
        }
        CloseConnection();
        statement.close();
        return auctions;

    }

    /**
     * this method is used to look for a all auctions of a specific seller.
     *
     * @param id   the id of the seller
     * @param type the specific Ah to look on
     * @return List of {@link Auction}
     * @throws SQLException                  sql handling
     * @throws GotDarnITFuckingUserException thrown when type does not match the static final values
     */
    public List<Auction> findAuctionsBySellerId(String type, int id) throws SQLException, GotDarnITFuckingUserException {
        correctAHValidation(type);
        List<Auction> auctions = new ArrayList<>();
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + type + " WHERE seller_id = ?");
        statement.setInt(1, id);
        statement.execute();
        ResultSet set = statement.getResultSet();
        if (set.next()) {
            auctions.add((Auction) setToData(set));
        }
        while (set.next()) {
            auctions.add((Auction) setToData(set));
        }
        CloseConnection();
        statement.close();
        return auctions;

    }

    /**
     * Method will look up a specific {@link Auction} and returns it.
     *
     * @param id   {@link Integer}
     * @param type the type of AH im looking on
     * @return {@link Auction}
     * @throws SQLException                  Sql handling
     * @throws GotDarnITFuckingUserException thrown when type does not match the static final values
     */
    public Auction findAuctionById(String type, int id) throws SQLException, GotDarnITFuckingUserException {
        correctAHValidation(type);
        Auction auction = new Auction();
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + type + " WHERE id = ?");
        statement.setInt(1, id);
        statement.execute();
        ResultSet set = statement.getResultSet();
        if (set.next()) {
            auction = (Auction) setToData(set);
        }
        CloseConnection();
        statement.close();
        return auction;
    }

    /**
     * @param set this is the resultset gotten by a method and makes a usable Object with it
     * @return {@link Object} more specifically {@link Auction}
     * @throws SQLException Sql handling
     */
    @Override
    protected Object setToData(ResultSet set) throws SQLException {
        Auction auction = new Auction();
        auction.setId(set.getInt(1));
        auction.setItemName(set.getString("itemName"));
        auction.setAmount(set.getInt("Amount"));
        Date date = set.getDate("postage_date");
        auction.setPostageDate(date);
        auction.setBuyoutPrice(set.getInt("buyout_price"));
        auction.setCurrentMinimalBid(set.getInt("current_minimal_bid"));
        auction.setBuyerID(set.getInt("buyer_id"));
        auction.setSellerID(set.getInt("seller_id"));
        return auction;
    }

    /**
     * This method will create a new entry on the database
     *
     * @param object {@link Auction}
     * @param type   String of what auctiontable to insert it in
     * @throws SQLException                  sql handling
     * @throws GotDarnITFuckingUserException thrown when type does not match the static final values
     */
    public void create(Object object, String type) throws SQLException, GotDarnITFuckingUserException {
        correctAHValidation(type);
        String query = "INSERT INTO " + type + " (seller_id, ItemName, Amount, buyout_price, current_minimal_bid, auctiontype_id)" +
                " values ( ?, ?, ?, ?, ?, ?)";
        Auction auctionInsert = (Auction) object;
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, auctionInsert.getSellerID());
        statement.setString(2, auctionInsert.getName());
        statement.setInt(3, auctionInsert.getAmount());
        statement.setInt(4, auctionInsert.getBuyoutPrice());
        statement.setInt(5, auctionInsert.getCurrentMinimalBid());
        statement.setInt(6, auctionInsert.getType());
        statement.execute();
        statement.close();
        CloseConnection();
    }

    /**
     * this method will delete a specific auction
     *
     * @param object {@link Auction}
     * @param type   string of what auctionTable to insert in
     * @throws SQLException                  sql handling
     * @throws GotDarnITFuckingUserException thrown when type does not match the static final values
     */
    public void delete(Object object, String type) throws SQLException, GotDarnITFuckingUserException {
        correctAHValidation(type);
        String query = "DELETE FROM " + type + " WHERE id = ?";
        Auction auctionInsert = (Auction) object;
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, auctionInsert.getId());
        statement.execute();
        statement.close();
        CloseConnection();
    }

    /**
     * this method will update the current minimal bid and buyer id on a specific auction
     *
     * @param replacement {@link Auction}
     * @param type        string of what auctionTable to insert in
     * @throws SQLException                  sql handling
     * @throws GotDarnITFuckingUserException thrown when type does not match the static final values
     */
    public void updateAuction(Object replacement, String type) throws SQLException, GotDarnITFuckingUserException {
        correctAHValidation(type);
        Auction auction = (Auction) replacement;
        String query = "UPDATE " + type + " SET " +
                "current_minimal_bid = ?, buyer_id = ?" +
                " WHERE id = ?";
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(3, auction.getId());
        statement.setInt(1, auction.getCurrentMinimalBid());
        statement.setInt(2, auction.getBuyerID());
        statement.execute();
        statement.close();
        CloseConnection();
    }
}
