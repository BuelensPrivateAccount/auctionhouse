package utilities;

import java.util.Date;

import static utilities.UserInputHandler.*;

public class UtilRequestingInfo {
    /**
     * private constructor to prevent instantiation
     */
    private UtilRequestingInfo() {
    }

    public static int requestMoney(String query) {
        System.out.println(query);
        int gold = fetchIntFromUser("you have not entered a valid input", "gold:");
        int silver = fetchIntFromUser("you have not entered a valid input", "silver:");
        int copper = fetchIntFromUser("you have not entered a valid input", "copper:");
        return gold*10000+silver*100+copper;
    }

    /**
     * requests a string from user
     *
     * @param query the question aimed at the user
     * @return {@link String}
     */
    public static String requestAString(String query) {
        return fetchStringFromUser("you have not entered a valid input", query);
    }

    /**
     * requests a integer from user
     *
     * @param query the question aimed at the user
     * @return {@link Integer}
     */
    public static int requestInt(String query) {
        return fetchIntFromUser("you have not entered a valid input", query);
    }

    /**
     * requests faction from the user.
     *
     * @return {@link Integer} 1 or 2 as valid faction
     */
    public static int requestFaction() {
        return fetchIntFromUser("you have not entered a valid input", "What faction do you want to join, type 1 for alliance, 2 for horde.");
    }

    /**
     * requests a username from the user
     *
     * @return {@link String}
     */
    public static String requestUsername() {
        return fetchStringFromUser("you have not entered a valid input.", "Username please:");
    }

    /**
     * requests a password from the user
     *
     * @return {@link String}
     */
    public static String requestPass() {
        return fetchStringFromUser("you have not entered a valid input.", "password please:");
    }

    /**
     * method asks the user to choose a option
     *
     * @return {@link Integer}
     */
    public static int chooseOption() {
        return fetchIntFromUser("You have not entered a valid input, please enter a number", "Please choose a option");

    }

    /**
     * method asks the user if they want to continue with the app
     *
     * @return {@link Boolean}
     */
    public static boolean requestContinue() {
        return fetchBooleanFromUser("you have not entered a valid input, please enter \"Yes\" or \"No\"", "Do you want to continue?");
    }

    /**
     * passthrough method to close the scanner.
     *
     * @see UserInputHandler#closeScannerMethod
     */
    public static void closeScanner() {
        closeScannerMethod();
    }


}
