package services;

import domain.Auction;
import exceptions.GotDarnITFuckingUserException;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.AuctionDAO;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class NeutralAuctionServiceTest {

    @Mock
    AuctionDAO auctionDAO;

    @InjectMocks
    NeutralAuctionService neutralAuctionService;

    @Test
    public void emptyTest() {
    }

    @DisplayName("Assert findAllAuctions returns the correct values")
    @Test
    public void findAllAuctionsTest() throws SQLException, GotDarnITFuckingUserException, NoRecordFoundException {
        Auction auction = new Auction();
        auction.setName("test");
        List<Auction> list = new ArrayList<>();
        list.add(auction);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        when(auctionDAO.findAllAuctions(argumentCaptor.capture())).thenReturn(list);
        List<Auction> output = neutralAuctionService.findAllAuctions();
        assertAll(() -> {
            assertEquals(AuctionDAO.NEUTRAL, argumentCaptor.getValue());
            assertEquals(list, output);
        });
    }

    @DisplayName("Assert that NoRecordFoundException is thrown when no records are find with FindAllAuctions")
    @Test
    public void findAllAuctionsTestThrowsNoRecordException() throws SQLException, GotDarnITFuckingUserException {
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        when(auctionDAO.findAllAuctions(argumentCaptor.capture())).thenReturn(new ArrayList<Auction>());
        assertAll(() -> {
            assertThrows(NoRecordFoundException.class, () -> {
                neutralAuctionService.findAllAuctions();
            });
            assertEquals(AuctionDAO.NEUTRAL, argumentCaptor.getValue());
        });
    }


    @DisplayName("Assert FindAuctionByQueryLikeness does return correct values")
    @Test
    public void findAuctionsByQueryLikenessTest() throws SQLException, GotDarnITFuckingUserException, NoRecordFoundException {
        Auction auction = new Auction();
        auction.setName("test");
        List<Auction> list = new ArrayList<>();
        list.add(auction);
        ArgumentCaptor<String> argumentCaptorStaticNeutral = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> argumentCaptorQuery = ArgumentCaptor.forClass(String.class);
        when(auctionDAO.findAuctionsByQueryLikeness(argumentCaptorStaticNeutral.capture(), argumentCaptorQuery.capture())).thenReturn(list);
        String s = "qwtyiop";
        List<Auction> output = neutralAuctionService.findAuctionsByQueryLikeness(s);
        assertAll(() -> {
            assertEquals(AuctionDAO.NEUTRAL, argumentCaptorStaticNeutral.getValue());
            assertEquals(list, output);
            assertEquals(s, argumentCaptorQuery.getValue());
        });
    }

    @DisplayName("Assert FindAuctionBySellerID does return correct values")
    @Test
    public void findAuctionsBySellerIDTest() throws SQLException, GotDarnITFuckingUserException, NoRecordFoundException {
        Auction auction = new Auction();
        auction.setName("test");
        List<Auction> list = new ArrayList<>();
        list.add(auction);
        ArgumentCaptor<String> argumentCaptorStaticNeutral = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> argumentCaptorQuery = ArgumentCaptor.forClass(Integer.class);
        when(auctionDAO.findAuctionsBySellerId(argumentCaptorStaticNeutral.capture(), argumentCaptorQuery.capture())).thenReturn(list);
        int s = 1;
        List<Auction> output = neutralAuctionService.findAuctionsBySellerID(s);
        assertAll(() -> {
            assertEquals(AuctionDAO.NEUTRAL, argumentCaptorStaticNeutral.getValue());
            assertEquals(list, output);
            assertEquals(s, argumentCaptorQuery.getValue());
        });
    }


    @DisplayName("Assert FindAuctionByQueryLikeness does throw NoRecordFoundException")
    @Test
    public void findAuctionsByQueryLikenessTestThrowsNoRecordException() throws SQLException, GotDarnITFuckingUserException {
        ArgumentCaptor<String> argumentCaptorStaticNeutral = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> argumentCaptorQuery = ArgumentCaptor.forClass(String.class);
        when(auctionDAO.findAuctionsByQueryLikeness(argumentCaptorStaticNeutral.capture(), argumentCaptorQuery.capture())).thenReturn(new ArrayList<>());
        String s = "qwtyiop";
        assertAll(() -> {
            assertThrows(NoRecordFoundException.class, () -> {
                neutralAuctionService.findAuctionsByQueryLikeness(s);
            });
            assertEquals(AuctionDAO.NEUTRAL, argumentCaptorStaticNeutral.getValue());
            assertEquals(s, argumentCaptorQuery.getValue());
        });
    }

    @DisplayName("Assert findAuctionById Works correctly")
    @Test
    public void findAuctionByIDTest() throws SQLException, GotDarnITFuckingUserException, NoRecordFoundException {
        Auction auction = new Auction();
        auction.setName("test");
        auction.setId(1);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        when(auctionDAO.findAuctionById(argumentCaptor.capture(), anyInt())).thenReturn(auction);
        int id = 123456;
        Auction output = neutralAuctionService.findAuctionById(id);
        assertAll(() -> {
            assertEquals(AuctionDAO.NEUTRAL, argumentCaptor.getValue());
            assertEquals(auction, output);
        });
    }

    @DisplayName("Assert findAuctionById throws NorecordException when no records are found")
    @Test
    public void findAuctionByIDTestThrowsNoRecordException() throws SQLException, GotDarnITFuckingUserException {
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        when(auctionDAO.findAuctionById(argumentCaptor.capture(), anyInt())).thenReturn(new Auction());
        assertAll(() -> {
            assertThrows(NoRecordFoundException.class, () -> {
                neutralAuctionService.findAuctionById(123);
            });
            assertEquals(AuctionDAO.NEUTRAL, argumentCaptor.getValue());
        });
    }

    @DisplayName("Assert createAuction Works correctly")
    @Test
    public void createAuctionTest() throws SQLException, GotDarnITFuckingUserException {
        Auction auction = new Auction();
        auction.setName("test");
        auction.setId(1);
        int id = 123456;
        neutralAuctionService.createNewAuction(auction);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(auctionDAO, times(1)).create(any(), argumentCaptor.capture());
        assertEquals(AuctionDAO.NEUTRAL,argumentCaptor.getValue());
    }

    @DisplayName("Assert updateAuction Works correctly")
    @Test
    public void updateAuctionTest() throws SQLException, GotDarnITFuckingUserException {
        Auction auction = new Auction();
        auction.setName("test");
        auction.setId(1);
        int id = 123456;
        neutralAuctionService.updateAuction(auction);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(auctionDAO, times(1)).updateAuction(any(), argumentCaptor.capture());
        assertEquals(AuctionDAO.NEUTRAL,argumentCaptor.getValue());
    }
    @DisplayName("Assert deleteAuction Works correctly")
    @Test
    public void deleteAuctionTest() throws SQLException, GotDarnITFuckingUserException {
        Auction auction = new Auction();
        auction.setName("test");
        auction.setId(1);
        int id = 123456;
        neutralAuctionService.deleteAuction(auction);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(auctionDAO, times(1)).delete(any(), argumentCaptor.capture());
        assertEquals(AuctionDAO.NEUTRAL,argumentCaptor.getValue());
    }
}