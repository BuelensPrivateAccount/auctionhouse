package services;

import domain.Player;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.PlayerDAO;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PlayerServiceTest {

    @Mock
    PlayerDAO playerDAO;

    @InjectMocks
    PlayerService playerService;

    @Test
    public void emptyTest() {
    }

    @DisplayName("Asserts that FindAllPlayers finds something.")
    @Test
    public void findAllPlayersTest() throws SQLException, NoRecordFoundException {
        Player player = new Player();
        player.setName("test");
        List<Player> list = new ArrayList<>();
        list.add(player);
        when(playerDAO.findAllPlayers()).thenReturn(list);
        List<Player> output = playerService.findAllPlayers();
        assertEquals(list, output);
    }

    @DisplayName("Asserts that norecordfound is thrown correctly")
    @Test
    public void findAllPlayersTestThrowNoRecordFoundException() throws SQLException {
        when(playerDAO.findAllPlayers()).thenReturn(new ArrayList<>());
        assertThrows(NoRecordFoundException.class, () -> {
            playerService.findAllPlayers();
        });
    }

    @DisplayName("Asserts that FindAllPlayers finds something.")
    @Test
    public void findAllAlliancePlayersTest() throws SQLException, NoRecordFoundException {
        Player player = new Player();
        player.setName("test");
        List<Player> list = new ArrayList<>();
        list.add(player);
        when(playerDAO.findAllAlliancePlayers()).thenReturn(list);
        List<Player> output = playerService.findAllAlliancePlayers();
        assertEquals(list, output);
    }

    @DisplayName("Asserts that norecordfound is thrown correctly")
    @Test
    public void findAllAlliancePlayersTestThrowNoRecordFoundException() throws SQLException {
        when(playerDAO.findAllAlliancePlayers()).thenReturn(new ArrayList<>());
        assertThrows(NoRecordFoundException.class, () -> {
            playerService.findAllAlliancePlayers();
        });
    }

    @DisplayName("Asserts that FindAllPlayers finds something.")
    @Test
    public void findAllHordePlayersTest() throws SQLException, NoRecordFoundException {
        Player player = new Player();
        player.setName("test");
        List<Player> list = new ArrayList<>();
        list.add(player);
        when(playerDAO.findAllHordePlayers()).thenReturn(list);
        List<Player> output = playerService.findAllHordePlayers();
        assertEquals(list, output);
    }

    @DisplayName("Asserts that NoRecordFoundException is thrown correctly")
    @Test
    public void findAllHordePlayersTestThrowNoRecordFoundException() throws SQLException {
        when(playerDAO.findAllHordePlayers()).thenReturn(new ArrayList<>());
        assertThrows(NoRecordFoundException.class, () -> {
            playerService.findAllHordePlayers();
        });
    }

    @DisplayName("Asserts that findPlayerById does return the correct player")
    @Test
    public void findPlayerByIDTest() throws SQLException, NoRecordFoundException {
        Player player = new Player();
        player.setName("test");
        player.setId(1);
        when(playerDAO.findPlayerByID(anyInt())).thenReturn(player);
        Player output = playerService.findPlayerByID(1);
        assertEquals(player, output);
    }

    @DisplayName("Asserts that findPlayerByName does return the correct player")
    @Test
    public void findPlayerByNameTest() throws SQLException, NoRecordFoundException {
        Player player = new Player();
        player.setName("test");
        player.setId(1);
        when(playerDAO.findSpecificPlayerByName(anyString())).thenReturn(player);
        Player output = playerService.findPlayerByName(player.getName());
        assertEquals(player, output);
    }

    @DisplayName("Asserts that NoRecordFoundException is thrown correctly")
    @Test
    public void findPlayerByIDTestThrowNoRecordFoundException() throws SQLException {
        when(playerDAO.findPlayerByID(anyInt())).thenReturn(new Player());
        assertThrows(NoRecordFoundException.class, () -> {
            playerService.findPlayerByID(123456);
        });
    }

    @DisplayName("Asserts that when LF player by partial name, it does return right player")
    @Test
    public void findPlayerWhereNameHasTest() throws SQLException, NoRecordFoundException {
        Player player = new Player();
        player.setName("test");
        List<Player> list = new ArrayList<>();
        list.add(player);
        String s = "test";
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        when(playerDAO.findPlayerWhereNameHas(argumentCaptor.capture())).thenReturn(list);
        List<Player> output = playerService.findPlayerWhereName(s);
        assertAll(() -> {
            assertEquals(s, argumentCaptor.getValue());
            assertEquals(list, output);
        });
    }

    @DisplayName("Asserts that when LF player by partial name and nothing is found, it throws the right exception")
    @Test
    public void findPlayerWhereNameHasThrowsExceptionTest() throws SQLException {
        String s = "test";
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        when(playerDAO.findPlayerWhereNameHas(argumentCaptor.capture())).thenReturn(new ArrayList<>());
        assertAll(() -> {
            assertThrows(NoRecordFoundException.class, () -> {
                playerService.findPlayerWhereName(s);
            });
            assertEquals(s, argumentCaptor.getValue());
        });
    }

    @DisplayName("Assert createPlayer Works correctly")
    @Test
    public void createPlayerTest() throws SQLException{
        Player player = new Player();
        player.setName("test");
        player.setId(1);
        playerService.createNewPlayer(player);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(playerDAO, times(1)).create(any());

    }

    @DisplayName("Assert updatePlayerMoney Works correctly")
    @Test
    public void updatePlayerMoneyTest() throws SQLException {
        Player player = new Player();
        player.setName("test");
        player.setId(1);
        playerService.updatePlayerMoney(player);
        verify(playerDAO, times(1)).updatePlayerMoney(any());
    }

    @DisplayName("Assert updatePlayerPass Works correctly")
    @Test
    public void updatePlayerPassTest() throws SQLException {
        Player player = new Player();
        player.setName("test");
        player.setId(1);
        playerService.updatePlayerPassword(player);
        verify(playerDAO, times(1)).updatePlayerPass(any());
    }

    @DisplayName("Assert deletePlayer Works correctly")
    @Test
    public void deletePlayerTest() throws SQLException {
        Player player = new Player();
        player.setName("test");
        player.setId(1);
        playerService.deletePlayer(player);
        verify(playerDAO, times(1)).delete(any());
    }
}